# Maintainer: Angel Velasquez <angvp@archlinux.org>
# Maintainer: Felix Yan <felixonmars@archlinux.org>
# Contributor: Douglas Soares de Andrade <douglas@archlinux.org>
# Contributor: Mario Danic <mario.danic@gmail.com>

pkgname=python-paramiko
pkgver=2.9.1
pkgrel=1
pkgdesc="Python module that implements the SSH2 protocol"
url="https://github.com/paramiko/paramiko/"
license=('LGPL')
arch=('any')
depends=('python-bcrypt' 'python-cryptography' 'python-pynacl')
makedepends=('python-setuptools' 'python-bcrypt' 'python-cryptography' 'python-pynacl')
checkdepends=('python-pytest-runner' 'python-pytest-relaxed' 'python-pyasn1' 'python-invoke'
             'python-mock')
source=("https://github.com/paramiko/paramiko/archive/$pkgver/$pkgname-$pkgver.tar.gz")
sha512sums=('d86824451be417322e0cdd073b31c103e737663af89f31abb1de21b5fbb9bdaa4b2e49505d2bc599114173df2457dcea308eb5c445fc5191248eb13e4c1eb500')

check() {
  cd paramiko-$pkgver
  LANG=en_US.UTF-8 python setup.py pytest
}

package() {
  cd paramiko-$pkgver

  python setup.py install --root="$pkgdir" --optimize=1
  install -dm755 "$pkgdir"/usr/share/doc/$pkgname/demos
  install -m644 demos/* "$pkgdir"/usr/share/doc/$pkgname/demos
  chmod 755 "$pkgdir"/usr/share/doc/$pkgname/demos/*.py
}
